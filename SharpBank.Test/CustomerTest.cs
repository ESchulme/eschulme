﻿using System;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            CheckingAccount checkingAccount = new CheckingAccount();
            SavingsAccount savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void CanFailWithdrawalPastZero()
        {
            var savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            savingsAccount.Deposit(400.0);
            savingsAccount.Deposit(100.0);

            try
            {
                savingsAccount.Withdraw(2000.0);
                Assert.Fail("Withdrawal amount is greater than balance");
            }
            catch(ArgumentException)
            {
                // nothing to do
            }
        }

        [Test]
        public void CanAdd()
        {
            var savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            savingsAccount.Deposit(400.0);
            savingsAccount.Deposit(100.0);

            try
            {
                savingsAccount.Deposit(0);
                Assert.Fail("Deposit amount is 0");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }
            try
            {
                savingsAccount.Deposit(-50);
                Assert.Fail("Deposit amount is < 0");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            savingsAccount.Deposit(200);

            Assert.AreEqual(savingsAccount.SumTransactions(), 700, "Incorrect Amount after Deposit");
        }

        [Test]
        public void CanWithdraw()
        {
            var savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            savingsAccount.Deposit(400.0);
            savingsAccount.Deposit(100.0);

            try
            {
                savingsAccount.Withdraw(0);
                Assert.Fail("Withdrawal amount is 0");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }
            try
            {
                savingsAccount.Withdraw(-50);
                Assert.Fail("Withdrawal amount is < 0");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            savingsAccount.Withdraw(200);

            Assert.AreEqual(savingsAccount.SumTransactions(), 300, "Incorrect Amount after Withdrawal");
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new SavingsAccount());
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());
            oscar.OpenAccount(new MaxiSavingsAccount());
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void CanTransfer()
        {
            var savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(500);

            var checkingAccount = new CheckingAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount);
            oscar.OpenAccount(checkingAccount);

            try
            {
                oscar.TransferMoney(null, checkingAccount, 200);
                Assert.Fail("'From' Account null - ArgumentNullException not thrown");
            }
            catch(ArgumentNullException)
            {
                // nothing to do
            }

            try
            {
                oscar.TransferMoney(savingsAccount, null, 200);
                Assert.Fail("'To' Account null - ArgumentNullException not thrown");
            }
            catch (ArgumentNullException)
            {
                // nothing to do
            }

            try
            {
                oscar.TransferMoney(savingsAccount, checkingAccount, -200);
                Assert.Fail("Negative amount - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            try
            {
                oscar.TransferMoney(savingsAccount, new MaxiSavingsAccount(), 200);
                Assert.Fail("Someone else's account - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            try
            {
                oscar.TransferMoney(new MaxiSavingsAccount(), savingsAccount, 200);
                Assert.Fail("Someone else's account - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            try
            {
                oscar.TransferMoney(savingsAccount, checkingAccount, 700);
                Assert.Fail("Excessive withdrawal - ArgumentException not thrown");
            }
            catch (ArgumentException)
            {
                // nothing to do
            }

            oscar.TransferMoney(savingsAccount, checkingAccount, 200);
            Assert.AreEqual(savingsAccount.SumTransactions(), 300, "Savings Account - invalid result");
            Assert.AreEqual(checkingAccount.SumTransactions(), 200, "Checking Account - invalid result");

        }
    }
}
