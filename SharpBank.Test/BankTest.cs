﻿using System;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new CheckingAccount());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            SavingsAccount savingsAccount = new SavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingsAccount));

            savingsAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            MaxiSavingsAccount maxiSavingsAccount = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            maxiSavingsAccount.Deposit(3000.0);

            Assert.AreEqual(170.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }


        [Test]
        public void CanGetFirstCustomer()
        {
            Bank bank = new Bank();
            bank.AddCustomer(new Customer("Bill"));
            bank.AddCustomer(new Customer("Ted"));
            bank.AddCustomer(new Customer("Alice"));

            string first = bank.GetFirstCustomer();

            Assert.AreEqual(first, "Bill", "Bank.GetFirstCustomer() mismatch");
        }

        [Test]
        public void CanFailAddNullCustomer()
        {
            Bank bank = new Bank();

            try
            {
                bank.AddCustomer(null);
                Assert.Fail("bank.AddCustomer() did not throw ArgumentNullException when receiving a NULL");
            }
            catch(ArgumentNullException)
            {

            }
        }

        [Test]
        public void CanGenerateCustomerSummary()
        {
            Bank bank = new Bank();
            string sSummary = bank.CustomerSummary();
            Assert.AreEqual(sSummary, "Customer Summary", "No customers - invalid");

            Customer customer = new Customer("Bill");
            bank.AddCustomer(customer);
            sSummary = bank.CustomerSummary();
            Assert.IsTrue(sSummary.ToLower().IndexOf("accounts") >= 0, "1 Customer 0 Accounts - not plural");

            customer.OpenAccount(new CheckingAccount());
            sSummary = bank.CustomerSummary();
            Assert.IsTrue(sSummary.ToLower().IndexOf("accounts") < 0, "1 Account - plural");

            customer.OpenAccount(new SavingsAccount());
            sSummary = bank.CustomerSummary();
            Assert.IsTrue(sSummary.ToLower().IndexOf("accounts") >= 0, "2 Accounts - not plural");
        }
    }
}
