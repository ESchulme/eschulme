﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public abstract class Account
    {
        #region Constants
        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;
        #endregion

        #region Properties
        public List<Transaction> transactions
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        protected Account()
        {
            this.transactions = new List<Transaction>();
        }
        #endregion

        #region Methods
        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            transactions.Add(new Transaction(amount));
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }

            if(amount > this.SumTransactions())
            {
                throw new ArgumentException("amount cannot be greater than balance");
            }
            transactions.Add(new Transaction(-amount));
        }

        public virtual double InterestEarned()
        {
            return this.SumTransactions();

            #region Moved to derived classes
            //switch (accountType)
            //{
            //    case SAVINGS:
            //        if (amount <= 1000)
            //            return amount * 0.001;
            //        else
            //            return 1 + (amount - 1000) * 0.002;
            //    // case SUPER_SAVINGS:
            //    //     if (amount <= 4000)
            //    //         return 20;
            //    case MAXI_SAVINGS:
            //        if (amount <= 1000)
            //            return amount * 0.02;
            //        if (amount <= 2000)
            //            return 20 + (amount - 1000) * 0.05;
            //        return 70 + (amount - 2000) * 0.1;
            //    default:
            //        return amount * 0.001;
            //}
            #endregion
        }

        public double SumTransactions()
        {
            return this.transactions.Sum(oTransaction => oTransaction.amount);
        }

        #region Unused
        //private double CheckIfTransactionsExist(bool checkAll)
        //{
        //    double amount = 0.0;
        //    foreach (Transaction t in transactions)
        //        amount += t.amount;
        //    return amount;
        //}
        #endregion

        public abstract int GetAccountType();
        #endregion

    }
}
