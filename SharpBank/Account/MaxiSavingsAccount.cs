﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavingsAccount : Account
    {
        #region Constructors
        public MaxiSavingsAccount()
        {

        }
        #endregion

        #region Methods
        public override int GetAccountType()
        {
            return Account.MAXI_SAVINGS;
        }

        public override double InterestEarned()
        {
            double dAmount = base.InterestEarned();
            if (dAmount <= 1000)
            {
                return dAmount * 0.02;
            }
            if (dAmount <= 2000)
            {
                return 20 + (dAmount - 1000) * 0.05;
            }

            return 70 + (dAmount - 2000) * 0.1;
        }
        #endregion
    }
}
