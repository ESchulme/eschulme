﻿namespace SharpBank
{
    public class SavingsAccount : Account
    {
        #region Constructors
        public SavingsAccount()
        {

        }
        #endregion


        #region Methods
        public override int GetAccountType()
        {
            return Account.SAVINGS;
        }

        public override double InterestEarned()
        {
            double dAmount = base.InterestEarned();
            if (dAmount <= 1000)
            {
                return dAmount * 0.001;
            }
            return 1 + (dAmount - 1000) * 0.002;
        }
        #endregion
    }
}
