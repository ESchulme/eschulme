﻿namespace SharpBank
{
    public class CheckingAccount : Account
    {
        #region Constructors
        public CheckingAccount()
        {

        }
        #endregion

        #region Methods
        public override int GetAccountType()
        {
            return Account.CHECKING;
        }

        public override double InterestEarned()
        {
            return base.InterestEarned() * 0.001;
        }

        #endregion
    }
}
